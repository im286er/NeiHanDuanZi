package com.neihanduanzi.model;

/**
 * Created by 齐泽威 on 2016/11/1.
 */

public class GroupId {
    private long group_id;

    public long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(long group_id) {
        this.group_id = group_id;
    }
}
