package com.neihanduanzi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EnterActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView reister;
    private EditText userName;
    private EditText userPass;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter);

        reister = (TextView) findViewById(R.id.register);
        userName = (EditText) findViewById(R.id.user_name);
        userPass = (EditText) findViewById(R.id.user_pass);
        login = (Button) findViewById(R.id.login_view);

        reister.setOnClickListener(this);
        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.register:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.login_view:
                String name = userName.getText().toString().trim();
                String passwd = userPass.getText().toString().trim();

                SharedPreferences preferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                String Sname = preferences.getString("userName", "h");
                String Spass = preferences.getString("userPass", "h");
                if (name.equals(Sname) && passwd.equals(Spass)){
                    Intent intent1 = new Intent(this, MainActivity.class);
                    startActivity(intent1);
                }else {
                    Toast.makeText(this, "用户名或密码匹配", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
}
