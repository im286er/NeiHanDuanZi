package com.neihanduanzi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Window;

import com.neihanduanzi.adapter.ViewPagerAdapter;
import com.neihanduanzi.animation.RotateDownPageTransformer;
import com.neihanduanzi.fragment.introfragments.Fragment1;
import com.neihanduanzi.fragment.introfragments.Fragment2;
import com.neihanduanzi.fragment.introfragments.Fragment3;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewPager 引导
 *
 */
public class ViewPagerActivity extends FragmentActivity {

	private ViewPager mVPActivity;
	private Fragment1 mFragment1;
	private Fragment2 mFragment2;
	private Fragment3 mFragment3;

	private List<Fragment> mListFragment = new ArrayList<Fragment>();
	private PagerAdapter mPgAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_viewpager);

		SharedPreferences preferences = getSharedPreferences("in", Context.MODE_PRIVATE);
		int flags = preferences.getInt("flags", 0);

		if (flags == 1){
			Intent intent = new Intent(this, WelcomeActivity.class);
			startActivity(intent);
			finish();
		}else {
			initView();
//			finish();
		}

	}



	private void initView() {
		mVPActivity = (ViewPager) findViewById(R.id.vp_activity);
		mFragment1 = new Fragment1();
		mFragment2 = new Fragment2();
		mFragment3 = new Fragment3();
		mListFragment.add(mFragment1);
		mListFragment.add(mFragment2);
		mListFragment.add(mFragment3);

		SharedPreferences in = getSharedPreferences("in", Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = in.edit();
		int flags = 1;
		edit.putInt("flags", flags);
		edit.commit();
		/**
		 * 传递两个参数
		 */
		mPgAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
				mListFragment);
		mVPActivity.setAdapter(mPgAdapter);
		mVPActivity.setPageTransformer(true, new RotateDownPageTransformer());
	}

}


